#!/bin/sh

IMAGE="$USER"/$(basename "$URL" .git)

git clone $URL && cd "$(basename "$URL" .git)" || exit

docker login -u "$USER" -p "$PASSWORD" "docker.io" || exit

docker build -t "$IMAGE" . || exit

docker push "$IMAGE":latest || exit

echo "\nImage can be found at https://hub.docker.com/r/$IMAGE"
