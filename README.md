# Part 1

## 1.1
```
CONTAINER ID        IMAGE               COMMAND                  CREATED              STATUS                      PORTS               NAMES
e85b1edb0a71        hello-world         "/hello"                 45 seconds ago       Exited (0) 44 seconds ago                       agitated_neumann
171137a1e59f        nginx               "nginx -g 'daemon of…"   56 seconds ago       Up 55 seconds               80/tcp              clever_heyrovsky
8b3d1b711742        nginx               "nginx -g 'daemon of…"   58 seconds ago       Up 57 seconds               80/tcp              unruffled_moser
626ac796a9de        nginx               "nginx -g 'daemon of…"   About a minute ago   Up About a minute           80/tcp              gifted_moore
```

## 1.2
```
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
```

## 1.3
input: basics

secret: "This is the secret message"

## 1.4
`$ docker run devopsdockeruh/exec_bash_exercise`

`$ docker exec -it stupefied_heisenberg /bin/bash`

`$ tail -f /usr/app/logs.txt`

Secret message is: "Docker is easy"

## 1.5
`docker run --rm -it --name ubuntu ubuntu:18.04 sh -c 'apt update && apt upgrade -y; apt install -y curl; echo "Input website:"; read website; echo "Searching.."; sleep 1; curl http://$website;'`

I added this part `apt update && apt upgrade -y; apt install -y curl;`

## 1.6
Command to run the container: `docker run -it docker-clock`

[Dockerfile](./1/1.6/Dockerfile)

## 1.7
`$ docker run -it curler`

[Script](./1/1.7/website.sh)

[Dockerfile](./1/1.7/Dockerfile)

## 1.8
```
$ touch logs.txt
$ docker run -v $(pwd)/logs.txt:/usr/app/logs.txt devopsdockeruh/first_volume_exercise
```

[logs.txt](./1/1.8/logs.txt)

## 1.9
`$ docker run -p 9011:80 devopsdockeruh/ports_exercise`

## 1.10
[Dockerfile](./1/1.10/Dockerfile)

## 1.11
```
$ touch logs.txt
$ docker run -v $(pwd)/logs.txt:/var/www/backend/logs.txt -p 8000:8000 backend:latest
```

[Dockerfile](./1/1.11/Dockerfile)

## 1.12
```
$ docker run -p 5000:5000 frontend:latest
$ docker run -v $(pwd)/logs.txt:/var/www/backend/logs.txt -p 8000:8000 backend:latest
```

[Dockerfile_front](./1/1.12/Dockerfile_front)

[Dockerfile_back](./1/1.12/Dockerfile_back)

## 1.13
[Dockerfile](./1/1.13/Dockerfile)

## 1.14
[Dockerfile](./1/1.14/Dockerfile)

## 1.15
URL to the [DockerHub repo](https://hub.docker.com/r/suruaku/course-python)

## 1.16
[Heroku app](https://devops-app-example.herokuapp.com/)

## 1.17
[My setup](https://hub.docker.com/r/suruaku/mysetup)

# Part 2

## 2.1
[docker-compose.yml](./2/2.1/docker-compose.yml)

[logs.txt](./2/2.1/logs.txt)

## 2.2
[docker-compose.yml](./2/2.2/docker-compose.yml)

## 2.3
[docker-compose.yml](./2/2.3/docker-compose.yml)

## 2.4
`$ docker-compose up --scale compute=4`

A little bit weird excercise since there is no buttons (so that the button in the application turns green.). Well the one with "Press here" text looks like it is for fome other excercise. It Did not change color even when I scaled up to 20.

## 2.5
[docker-compose.yml](./2/2.5/docker-compose.yml)

## 2.6
[docker-compose.yml](./2/2.6/docker-compose.yml)

## 2.7
[docker-compose.yml](./2/2.7/docker-compose.yaml)

## 2.8
[docker-compose.yml](./2/2.8/docker-compose.yml)

[nginx.conf](./2/2.8/nginx.conf)

## 2.9
[docker-compose.yml](./2/2.9/docker-compose.yml)

## 2.10
Everything still works.

# Part 3

## 3.1
frontend 458MB -> 430MB

backend 362MB -> 334MB

[Dockerfile_front](./3/3.1/Dockerfile_front)

[Dockerfile_back](./3/3.1/Dockerfile_back)

## 3.2
If this is still empty then I did not do it.

## 3.3
[Image](https://hub.docker.com/repository/docker/suruaku/builder)
[Dockerfiler](./3/3.3/Dockerfile)
[script](./3/3.3/builder.sh)
Create [env.list](./3/3.3/env.list) file. with variables `USERNAME` & `PASSWORD` for dockerhub and `URL`for git project to work with.
`$ docker run --privileged --env-file env.list -v /var/run/docker.sock:/var/run/docker.sock suruaku/builder:latest`

## 3.4
[Dockerfile_front](./3/3.4/Dockerfile_front)

[Dockerfile_back](./3/3.4/Dockerfile_back)

## 3.5
frontend 430MB -> 244MB

backend 334MB -> 147MB

[Dockerfile.alpine](./3/3.5/Dockerfile.alpine)

[Dockerfile_front](./3/3.5/Dockerfile_front)

[Dockerfile_back](./3/3.5/Dockerfile_back)

## 3.6
frontend 244MB -> 97.5MB

[Dockerfile](./3/3.6/Dockerfile)

## 3.7
[Dockerfile before](./3/3.7/Dockerfile_old)

[Dockerfile after](./3/3.7/Dockerfile_new)

## 3.8
![Diagram](./3/3.8/kube.jpg)

And why not to include a real diagram of my own kubernetes cluster running on a bare metal server.
![Diagram](./3/3.8/omakube.png)

![Diagram](./3/3.8/omakube2.png)
